package ru.baulina.tm.util;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

    static Integer nexInt() {
        final String value = nextLine();
        return Integer.parseInt(value);
    }

    static Long nexLong() {
        final String value = nextLine();
        return Long.parseLong(value);
    }

}
