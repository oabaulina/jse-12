package ru.baulina.tm.api.repository;

import ru.baulina.tm.model.Project;
import ru.baulina.tm.model.Task;

import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(Long id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(Long id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

}
