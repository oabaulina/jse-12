package ru.baulina.tm.api.service;

import ru.baulina.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task task);

    void remove(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(Long id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(Long id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateTaskById(Long id, String name, String  description);

    Task updateTaskByIndex(Integer index, String name, String  description);

}
