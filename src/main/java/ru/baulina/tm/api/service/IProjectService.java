package ru.baulina.tm.api.service;

import ru.baulina.tm.model.Project;
import ru.baulina.tm.model.Task;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    void clear();

    Project findOneById(Long id);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneById(Long id);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateTaskById(Long id, String name, String  description);

    Project updateTaskByIndex(Integer index, String name, String  description);

}
