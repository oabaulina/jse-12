package ru.baulina.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showInfo();

    void displayWelcome();

    void exit();

}
