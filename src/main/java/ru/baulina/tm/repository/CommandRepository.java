package ru.baulina.tm.repository;

import ru.baulina.tm.api.repository.ICommandRepository;
import ru.baulina.tm.constant.ArgumentConst;
import ru.baulina.tm.constant.CommandConst;
import ru.baulina.tm.constant.CommandDescriptionConst;
import ru.baulina.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP, CommandDescriptionConst.HELP
    );

    public static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT, CommandDescriptionConst.ABOUT
    );

    public static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION, CommandDescriptionConst.VERSION
    );

    public static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO, CommandDescriptionConst.INFO
    );

    public static final Command EXIT = new Command(
            CommandConst.EXIT, null, CommandDescriptionConst.EXIT
    );

    public static final Command ARGUMENT = new Command(
            CommandConst.ARGUMENTS, ArgumentConst.ARGUMENTS, CommandDescriptionConst.ARGUMENTS
    );

    public static final Command COMMAND = new Command(
            CommandConst.COMMANDS, ArgumentConst.COMMANDS, CommandDescriptionConst.COMMANDS
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null, CommandDescriptionConst.TASK_CREATE
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null, CommandDescriptionConst.TASK_LIST
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null, CommandDescriptionConst.TASK_CLEAR
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            CommandConst.TASK_UPDATE_BY_ID, null, CommandDescriptionConst.TASK_UPDATE_BY_ID
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            CommandConst.TASK_UPDATE_BY_INDEX, null, CommandDescriptionConst.TASK_UPDATE_BY_INDEX
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            CommandConst.TASK_VIEW_BY_ID, null, CommandDescriptionConst.TASK_VIEW_BY_ID
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            CommandConst.TASK_VIEW_BY_INDEX, null, CommandDescriptionConst.TASK_VIEW_BY_INDEX
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            CommandConst.TASK_VIEW_BY_NAME, null, CommandDescriptionConst.TASK_VIEW_BY_NAME
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            CommandConst.TASK_REMOVE_BY_ID, null, CommandDescriptionConst.TASK_REMOVE_BY_ID
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            CommandConst.TASK_REMOVE_BY_INDEX, null, CommandDescriptionConst.TASK_REMOVE_BY_INDEX
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            CommandConst.TASK_REMOVE_BY_NAME, null, CommandDescriptionConst.TASK_REMOVE_BY_NAME
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null, CommandDescriptionConst.PROJECT_CREATE
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null, CommandDescriptionConst.PROJECT_LIST
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null, CommandDescriptionConst.PROJECT_CLEAR
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            CommandConst.PROJECT_UPDATE_BY_ID, null, CommandDescriptionConst.PROJECT_UPDATE_BY_ID
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            CommandConst.PROJECT_UPDATE_BY_INDEX, null, CommandDescriptionConst.PROJECT_UPDATE_BY_INDEX
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            CommandConst.PROJECT_VIEW_BY_ID, null, CommandDescriptionConst.PROJECT_VIEW_BY_ID
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            CommandConst.PROJECT_VIEW_BY_INDEX, null, CommandDescriptionConst.PROJECT_VIEW_BY_INDEX
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            CommandConst.PROJECT_VIEW_BY_NAME, null, CommandDescriptionConst.PROJECT_VIEW_BY_NAME
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            CommandConst.PROJECT_REMOVE_BY_ID, null, CommandDescriptionConst.PROJECT_REMOVE_BY_ID
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            CommandConst.PROJECT_REMOVE_BY_INDEX, null, CommandDescriptionConst.PROJECT_REMOVE_BY_INDEX
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            CommandConst.PROJECT_REMOVE_BY_NAME, null, CommandDescriptionConst.PROJECT_REMOVE_BY_NAME
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            HELP, ABOUT, VERSION, ARGUMENT, COMMAND, INFO,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX, TASK_VIEW_BY_NAME,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX, TASK_REMOVE_BY_NAME ,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_INDEX, PROJECT_VIEW_BY_NAME,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX, PROJECT_REMOVE_BY_NAME,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }

        return  Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[] {};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }

        return  Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return  TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
