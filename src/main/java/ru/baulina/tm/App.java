package ru.baulina.tm;

import ru.baulina.tm.bootstrap.Bootstrap;

public class App {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}


